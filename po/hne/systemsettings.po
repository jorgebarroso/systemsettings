# translation of systemsettings.po to Hindi
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Ravishankar Shrivastava <raviratlami@yahoo.com>, 2007.
# Ravishankar Shrivastava <raviratlami@aol.in>, 2009.
msgid ""
msgstr ""
"Project-Id-Version: systemsettings\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-01 23:30+0000\n"
"PO-Revision-Date: 2009-01-27 13:48+0530\n"
"Last-Translator: Ravishankar Shrivastava <raviratlami@aol.in>\n"
"Language-Team: Hindi <kde-i18n-doc@lists.kde.org>\n"
"Language: hne\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"
"X-Generator: Lokalize 0.2\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "रविसंकर सिरीवास्तव, जी. करूनाकर"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "raviratlami@aol.in,"

#: app/main.cpp:61 app/SettingsBase.cpp:65
#, kde-format
msgid "Info Center"
msgstr ""

#: app/main.cpp:63
#, kde-format
msgid "Centralized and convenient overview of system information."
msgstr ""

#: app/main.cpp:65 app/main.cpp:76 icons/IconMode.cpp:58
#, kde-format
msgid "(c) 2009, Ben Cooksley"
msgstr ""

#: app/main.cpp:72 app/SettingsBase.cpp:68 runner/systemsettingsrunner.cpp:175
#: sidebar/package/contents/ui/introPage.qml:63
#, kde-format
msgid "System Settings"
msgstr "तंत्र सेटिंग"

#: app/main.cpp:74
#, kde-format
msgid "Central configuration center by KDE."
msgstr ""

#: app/main.cpp:87 icons/IconMode.cpp:59 sidebar/SidebarMode.cpp:171
#, kde-format
msgid "Ben Cooksley"
msgstr ""

#: app/main.cpp:87
#, kde-format
msgid "Maintainer"
msgstr ""

#: app/main.cpp:88 icons/IconMode.cpp:60 sidebar/SidebarMode.cpp:172
#, kde-format
msgid "Mathias Soeken"
msgstr ""

#: app/main.cpp:88 icons/IconMode.cpp:60 sidebar/SidebarMode.cpp:172
#, kde-format
msgid "Developer"
msgstr ""

#: app/main.cpp:89
#, kde-format
msgid "Will Stephenson"
msgstr ""

#: app/main.cpp:89
#, kde-format
msgid "Internal module representation, internal module model"
msgstr ""

#: app/main.cpp:97
#, kde-format
msgid "List all possible modules"
msgstr ""

#: app/main.cpp:98 app/main.cpp:161
#, kde-format
msgid "Configuration module to open"
msgstr ""

#: app/main.cpp:99 app/main.cpp:162
#, fuzzy, kde-format
#| msgid "About Current Module"
msgid "Arguments for the module"
msgstr "अभी हाल के माड्यूल के बारे में"

#: app/main.cpp:107
#, kde-format
msgid "The following modules are available:"
msgstr ""

#: app/main.cpp:125
#, kde-format
msgid "No description available"
msgstr ""

#: app/SettingsBase.cpp:59
#, fuzzy, kde-format
#| msgid "S&earch:"
msgctxt "Search through a list of control modules"
msgid "Search"
msgstr "खोजव: (&e)"

#: app/SettingsBase.cpp:156
#, kde-format
msgid "Switch to Icon View"
msgstr ""

#: app/SettingsBase.cpp:163
#, kde-format
msgid "Switch to Sidebar View"
msgstr ""

#: app/SettingsBase.cpp:172
#, kde-format
msgid "Highlight Changed Settings"
msgstr ""

#: app/SettingsBase.cpp:181
#, kde-format
msgid "Report a Bug in the Current Page…"
msgstr ""

#: app/SettingsBase.cpp:208
#, kde-format
msgid "Help"
msgstr ""

#: app/SettingsBase.cpp:365
#, kde-format
msgid ""
"System Settings was unable to find any views, and hence has nothing to "
"display."
msgstr ""

#: app/SettingsBase.cpp:366
#, kde-format
msgid "No views found"
msgstr ""

#: app/SettingsBase.cpp:424
#, fuzzy, kde-format
#| msgid "About Current Module"
msgid "About Active View"
msgstr "अभी हाल के माड्यूल के बारे में"

#: app/SettingsBase.cpp:495
#, fuzzy, kde-format
#| msgctxt "Help menu->about <modulename>"
#| msgid "About %1"
msgid "About %1"
msgstr "%1 के बारे में"

#. i18n: ectx: label, entry (ActiveView), group (Main)
#: app/systemsettings.kcfg:9
#, kde-format
msgid "Internal name for the view used"
msgstr ""

#. i18n: ectx: ToolBar (mainToolBar)
#: app/systemsettingsui.rc:15
#, fuzzy, kde-format
#| msgid "System Settings"
msgid "About System Settings"
msgstr "तंत्र सेटिंग"

#: app/ToolTips/tooltipmanager.cpp:188
#, kde-format
msgid "Contains 1 item"
msgid_plural "Contains %1 items"
msgstr[0] ""
msgstr[1] ""

#: core/ExternalAppModule.cpp:27
#, kde-format
msgid "%1 is an external application and has been automatically launched"
msgstr ""

#: core/ExternalAppModule.cpp:28
#, kde-format
msgid "Relaunch %1"
msgstr ""

#. i18n: ectx: property (windowTitle), widget (QWidget, ExternalModule)
#: core/externalModule.ui:14
#, kde-format
msgid "Dialog"
msgstr ""

#: core/ModuleView.cpp:172
#, kde-format
msgid "Reset all current changes to previous values"
msgstr ""

#: core/ModuleView.cpp:318
#, fuzzy, kde-format
#| msgid ""
#| "There are unsaved changes in the active module.\n"
#| "Do you want to apply the changes or discard them?"
msgid ""
"The settings of the current module have changed.\n"
"Do you want to apply the changes or discard them?"
msgstr ""
"सक्रिय माड्यूल मं बिन सहेजे गे बदलाव हे.\n"
"का आप मन बदलाव मन ल लागू करना चाहथो या ओ मन ल छोड़व?"

#: core/ModuleView.cpp:320
#, fuzzy, kde-format
#| msgid "System Settings"
msgid "Apply Settings"
msgstr "तंत्र सेटिंग"

#: icons/IconMode.cpp:54
#, kde-format
msgid "Icon View"
msgstr ""

#: icons/IconMode.cpp:56
#, kde-format
msgid "Provides a categorized icons view of control modules."
msgstr ""

#: icons/IconMode.cpp:59 sidebar/SidebarMode.cpp:170
#: sidebar/SidebarMode.cpp:171
#, kde-format
msgid "Author"
msgstr "लेखक"

#: icons/IconMode.cpp:63
#, fuzzy, kde-format
#| msgid "System Settings"
msgid "All Settings"
msgstr "तंत्र सेटिंग"

#: icons/IconMode.cpp:64
#, kde-format
msgid "Keyboard Shortcut: %1"
msgstr ""

#: runner/systemsettingsrunner.cpp:40
#, kde-format
msgid "Finds system settings modules whose names or descriptions match :q:"
msgstr ""

#: runner/systemsettingsrunner.cpp:173
#, fuzzy, kde-format
#| msgid "System Settings"
msgid "System Information"
msgstr "तंत्र सेटिंग"

#: sidebar/package/contents/ui/CategoriesPage.qml:58
#, kde-format
msgid "Show intro page"
msgstr ""

#: sidebar/package/contents/ui/CategoriesPage.qml:101
#, kde-format
msgctxt "A search yielded no results"
msgid "No items matching your search"
msgstr ""

#: sidebar/package/contents/ui/HamburgerMenuButton.qml:25
#, kde-format
msgid "Show menu"
msgstr ""

#: sidebar/package/contents/ui/introPage.qml:56
#, kde-format
msgid "Plasma"
msgstr ""

#: sidebar/SidebarMode.cpp:165
#, kde-format
msgid "Sidebar View"
msgstr ""

#: sidebar/SidebarMode.cpp:167
#, kde-format
msgid "Provides a categorized sidebar for control modules."
msgstr ""

#: sidebar/SidebarMode.cpp:169
#, kde-format
msgid "(c) 2017, Marco Martin"
msgstr ""

#: sidebar/SidebarMode.cpp:170
#, kde-format
msgid "Marco Martin"
msgstr ""

#: sidebar/SidebarMode.cpp:650
#, kde-format
msgid "Sidebar"
msgstr ""

#: sidebar/SidebarMode.cpp:721
#, kde-format
msgid "Most Used"
msgstr ""

#, fuzzy
#~| msgid "Configure"
#~ msgid "Configure…"
#~ msgstr "कान्फिगर"

#, fuzzy
#~| msgid "About Current Module"
#~ msgid "About Active Module"
#~ msgstr "अभी हाल के माड्यूल के बारे में"

#, fuzzy
#~| msgid "Configure"
#~ msgid "Configure your system"
#~ msgstr "कान्फिगर"

#, fuzzy
#~| msgid "S&earch:"
#~ msgid "Search..."
#~ msgstr "खोजव: (&e)"

#, fuzzy
#~| msgid "System Settings"
#~ msgid "System Settings Handbook"
#~ msgstr "तंत्र सेटिंग"

#, fuzzy
#~| msgctxt "Help menu->about <modulename>"
#~| msgid "About %1"
#~ msgid "About KDE"
#~ msgstr "%1 के बारे में"

#, fuzzy
#~| msgid "Overview (%1)"
#~ msgid "Overview"
#~ msgstr "ओवरव्यू (%1)"

#, fuzzy
#~| msgid "About Current Module"
#~ msgid "About current module"
#~ msgstr "अभी हाल के माड्यूल के बारे में"

#~ msgid "(c) 2005, Benjamin C. Meyer; (c) 2007, Canonical Ltd"
#~ msgstr "(c) 2005, बेंजामिन सी मेयर; (c) 2007, केनानिकल लि."

#~ msgid "Benjamin C. Meyer"
#~ msgstr "बेंजामिन सी मेयर"

#~ msgid "Jonathan Riddell"
#~ msgstr "जोनाथन रिडेल"

#~ msgid "Contributor"
#~ msgstr "सहयोगी"

#~ msgid "Michael D. Stemle"
#~ msgstr "माइकल डी स्टेमल"

#~ msgid "Simon Edwards"
#~ msgstr "साइमन एडवर्ड्स"

#~ msgid "Ellen Reitmayr"
#~ msgstr "एलन रेतमायर"

#~ msgid "Usability"
#~ msgstr "उपयोगिता"

#~ msgid "Search Bar<p>Enter a search term.</p>"
#~ msgstr "खोजक पट्टी<p>खोजे के कोनो वाक्यांस भरव.</p>"

#~ msgid "%1 hit in General"
#~ msgid_plural "%1 hits in General"
#~ msgstr[0] "आमतौर मं %1 हिट"
#~ msgstr[1] "आमतौर मं %1 हिट्स"

#~ msgid "%1 hit in Advanced"
#~ msgid_plural "%1 hits in Advanced"
#~ msgstr[0] "उन्नत रूप मं %1 हिट"
#~ msgstr[1] "उन्नत रूप मं %1 हिट्स"

#~ msgid "Unsaved Changes"
#~ msgstr "बिन सहेजे बदलाव"

#~ msgid "Undo Changes"
#~ msgstr "बदलाव मन ल पूर्ववत् करव"

#~ msgid "Reset to Defaults"
#~ msgstr "डिफाल्ट मं रीसेट करव"

#~ msgid "Ctrl+O"
#~ msgstr "कंट्रोल+O"

#~ msgid "&File"
#~ msgstr "फाइल (&F)"

#~ msgid "&View"
#~ msgstr "देखव (&V)"

#~ msgid "Menu file"
#~ msgstr "मेन्यू फाइल"
