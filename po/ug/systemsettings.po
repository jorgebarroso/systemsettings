# Uyghur translation for systemsettings.
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Sahran <sahran.ug@gmail.com>, 2011.
#
msgid ""
msgstr ""
"Project-Id-Version: systemsettings\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-01 23:30+0000\n"
"PO-Revision-Date: 2013-09-08 07:05+0900\n"
"Last-Translator: Gheyret Kenji <gheyret@gmail.com>\n"
"Language-Team: Uyghur Computer Science Association <UKIJ@yahoogroups.com>\n"
"Language: ug\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "ئابدۇقادىر ئابلىز, غەيرەت كەنجى"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "sahran.ug@gmail.com,  gheyret@gmail.com"

#: app/main.cpp:61 app/SettingsBase.cpp:65
#, kde-format
msgid "Info Center"
msgstr ""

#: app/main.cpp:63
#, kde-format
msgid "Centralized and convenient overview of system information."
msgstr ""

#: app/main.cpp:65 app/main.cpp:76 icons/IconMode.cpp:58
#, kde-format
msgid "(c) 2009, Ben Cooksley"
msgstr "(c) 2009, Ben Cooksley"

#: app/main.cpp:72 app/SettingsBase.cpp:68 runner/systemsettingsrunner.cpp:175
#: sidebar/package/contents/ui/introPage.qml:63
#, kde-format
msgid "System Settings"
msgstr "سىستېما تەڭشەكلىرى"

#: app/main.cpp:74
#, fuzzy, kde-format
#| msgid "Central configuration center for KDE."
msgid "Central configuration center by KDE."
msgstr "ك د ئې(KDE) نىڭ سەپلەش مەركىزى."

#: app/main.cpp:87 icons/IconMode.cpp:59 sidebar/SidebarMode.cpp:171
#, kde-format
msgid "Ben Cooksley"
msgstr "Ben Cooksley"

#: app/main.cpp:87
#, kde-format
msgid "Maintainer"
msgstr "مەسئۇل كىشى"

#: app/main.cpp:88 icons/IconMode.cpp:60 sidebar/SidebarMode.cpp:172
#, kde-format
msgid "Mathias Soeken"
msgstr "Mathias Soeken"

#: app/main.cpp:88 icons/IconMode.cpp:60 sidebar/SidebarMode.cpp:172
#, kde-format
msgid "Developer"
msgstr "ئىجادىيەتچى"

#: app/main.cpp:89
#, kde-format
msgid "Will Stephenson"
msgstr "Will Stephenson"

#: app/main.cpp:89
#, kde-format
msgid "Internal module representation, internal module model"
msgstr "ئىچكى بۆلەكنىڭ چۈشەندۈرۈلۈشى، ئىچكى بۆلەكنىڭ مودېلى"

#: app/main.cpp:97
#, kde-format
msgid "List all possible modules"
msgstr ""

#: app/main.cpp:98 app/main.cpp:161
#, kde-format
msgid "Configuration module to open"
msgstr ""

#: app/main.cpp:99 app/main.cpp:162
#, kde-format
msgid "Arguments for the module"
msgstr ""

#: app/main.cpp:107
#, kde-format
msgid "The following modules are available:"
msgstr ""

#: app/main.cpp:125
#, kde-format
msgid "No description available"
msgstr ""

#: app/SettingsBase.cpp:59
#, kde-format
msgctxt "Search through a list of control modules"
msgid "Search"
msgstr "ئىزدە"

#: app/SettingsBase.cpp:156
#, fuzzy, kde-format
#| msgid "Icon View"
msgid "Switch to Icon View"
msgstr "سىنبەلگە كۆرۈنۈش"

#: app/SettingsBase.cpp:163
#, kde-format
msgid "Switch to Sidebar View"
msgstr ""

#: app/SettingsBase.cpp:172
#, kde-format
msgid "Highlight Changed Settings"
msgstr ""

#: app/SettingsBase.cpp:181
#, kde-format
msgid "Report a Bug in the Current Page…"
msgstr ""

#: app/SettingsBase.cpp:208
#, kde-format
msgid "Help"
msgstr "ياردەم"

#: app/SettingsBase.cpp:365
#, kde-format
msgid ""
"System Settings was unable to find any views, and hence has nothing to "
"display."
msgstr ""
"سىستېما تەڭشىكى ھېچقانداق كۆرۈنۈشلەرنى تاپالمىدى. شۇڭا كۆرسىتىش ئېلىپ "
"بارغىلى بولمايدۇ."

#: app/SettingsBase.cpp:366
#, kde-format
msgid "No views found"
msgstr "كۆرۈنۈشلەر تېپىلمىدى"

#: app/SettingsBase.cpp:424
#, kde-format
msgid "About Active View"
msgstr "ئاكتىپ كۆرۈنۈش ھەققىدە"

#: app/SettingsBase.cpp:495
#, kde-format
msgid "About %1"
msgstr "%1 ھەققىدە"

#. i18n: ectx: label, entry (ActiveView), group (Main)
#: app/systemsettings.kcfg:9
#, kde-format
msgid "Internal name for the view used"
msgstr "ئىشلىتىلگەن كۆرۈنۈشنىڭ ئىچكى ئاتى."

#. i18n: ectx: ToolBar (mainToolBar)
#: app/systemsettingsui.rc:15
#, kde-format
msgid "About System Settings"
msgstr "سىستېما تەڭشەكلىرى ھەققىدە"

#: app/ToolTips/tooltipmanager.cpp:188
#, fuzzy, kde-format
#| msgid "<i>Contains 1 item</i>"
#| msgid_plural "<i>Contains %1 items</i>"
msgid "Contains 1 item"
msgid_plural "Contains %1 items"
msgstr[0] "<i>%1 تۈر بار</i>"

#: core/ExternalAppModule.cpp:27
#, kde-format
msgid "%1 is an external application and has been automatically launched"
msgstr "%1 سىرتقى پروگرامما بولۇپ ئاپتوماتىك ئىجرا قىلىنغان"

#: core/ExternalAppModule.cpp:28
#, kde-format
msgid "Relaunch %1"
msgstr "قايتا ئىجرا قىل %1"

#. i18n: ectx: property (windowTitle), widget (QWidget, ExternalModule)
#: core/externalModule.ui:14
#, kde-format
msgid "Dialog"
msgstr "سۆزلەشكۈ"

#: core/ModuleView.cpp:172
#, kde-format
msgid "Reset all current changes to previous values"
msgstr "ئالدىنقى قېتىمقى قىممەتلىرىگە قايتۇر"

#: core/ModuleView.cpp:318
#, kde-format
msgid ""
"The settings of the current module have changed.\n"
"Do you want to apply the changes or discard them?"
msgstr ""
"نۆۋەتتىكى بۆلەكنىڭ تەڭشىكى ئۆزگەرتىلدى.\n"
"ئۆزگەرتىشنى ساقلامسىز ياكى تاشلىۋېتەمسىز؟"

#: core/ModuleView.cpp:320
#, kde-format
msgid "Apply Settings"
msgstr "تەڭشەك قوللان"

#: icons/IconMode.cpp:54
#, kde-format
msgid "Icon View"
msgstr "سىنبەلگە كۆرۈنۈش"

#: icons/IconMode.cpp:56
#, kde-format
msgid "Provides a categorized icons view of control modules."
msgstr ""
"تىزگىنلەش بۆلەكلىرىنىڭ كاتېگورىيەگە ئايرىلغان، سىنبەلگىلىك كۆرۈنۈشىنى "
"تەمىنلەيدۇ."

#: icons/IconMode.cpp:59 sidebar/SidebarMode.cpp:170
#: sidebar/SidebarMode.cpp:171
#, kde-format
msgid "Author"
msgstr "ئاپتور"

#: icons/IconMode.cpp:63
#, fuzzy, kde-format
#| msgid "Apply Settings"
msgid "All Settings"
msgstr "تەڭشەك قوللان"

#: icons/IconMode.cpp:64
#, kde-format
msgid "Keyboard Shortcut: %1"
msgstr "ھەرپتاختا تېزلەتمىسى: %1"

#: runner/systemsettingsrunner.cpp:40
#, kde-format
msgid "Finds system settings modules whose names or descriptions match :q:"
msgstr ""

#: runner/systemsettingsrunner.cpp:173
#, fuzzy, kde-format
#| msgid "System Settings"
msgid "System Information"
msgstr "سىستېما تەڭشەكلىرى"

#: sidebar/package/contents/ui/CategoriesPage.qml:58
#, kde-format
msgid "Show intro page"
msgstr ""

#: sidebar/package/contents/ui/CategoriesPage.qml:101
#, kde-format
msgctxt "A search yielded no results"
msgid "No items matching your search"
msgstr ""

#: sidebar/package/contents/ui/HamburgerMenuButton.qml:25
#, kde-format
msgid "Show menu"
msgstr ""

#: sidebar/package/contents/ui/introPage.qml:56
#, kde-format
msgid "Plasma"
msgstr ""

#: sidebar/SidebarMode.cpp:165
#, kde-format
msgid "Sidebar View"
msgstr ""

#: sidebar/SidebarMode.cpp:167
#, fuzzy, kde-format
#| msgid "Provides a categorized icons view of control modules."
msgid "Provides a categorized sidebar for control modules."
msgstr ""
"تىزگىنلەش بۆلەكلىرىنىڭ كاتېگورىيەگە ئايرىلغان، سىنبەلگىلىك كۆرۈنۈشىنى "
"تەمىنلەيدۇ."

#: sidebar/SidebarMode.cpp:169
#, kde-format
msgid "(c) 2017, Marco Martin"
msgstr ""

#: sidebar/SidebarMode.cpp:170
#, kde-format
msgid "Marco Martin"
msgstr ""

#: sidebar/SidebarMode.cpp:650
#, kde-format
msgid "Sidebar"
msgstr ""

#: sidebar/SidebarMode.cpp:721
#, kde-format
msgid "Most Used"
msgstr ""

#~ msgid "<i>Contains 1 item</i>"
#~ msgid_plural "<i>Contains %1 items</i>"
#~ msgstr[0] "<i>%1 تۈر بار</i>"

#~ msgid "View Style"
#~ msgstr "كۆرۈنۈش ئۇسلۇبى"

#~ msgid "Show detailed tooltips"
#~ msgstr "تەپسىلىي كۆرسەتمىلەرنى كۆرسەت"

#, fuzzy
#~| msgid "Configure"
#~ msgid "Configure…"
#~ msgstr "سەپلە"

#~ msgctxt "General config for System Settings"
#~ msgid "General"
#~ msgstr "ئادەتتىكى"

#~ msgid ""
#~ "System Settings was unable to find any views, and hence nothing is "
#~ "available to configure."
#~ msgstr ""
#~ "سىستېما تەڭشىكى ھېچقانداق كۆرۈنۈشلەرنى تاپالمىدى. شۇڭا سەپلەش ئېلىپ "
#~ "بارغىلى بولمايدۇ."

#~ msgid "Determines whether detailed tooltips should be used"
#~ msgstr "تەپسىلىي كۆرسەتمىلەر ئىشلىتىلەمدۇ يوق بەلگىلەيدۇ."

#~ msgid "About Active Module"
#~ msgstr "ئاكتىپ بۆلەكلەر ھەققىدە"

#~ msgid "Configure your system"
#~ msgstr "سىستېمىنى سەپلەڭ"

#~ msgid ""
#~ "Welcome to \"System Settings\", a central place to configure your "
#~ "computer system."
#~ msgstr ""
#~ "«سىستېما تەڭشىكى»گە مەرھابا. بۇ يەر كومپيۇتېر سىستېمىڭىزنى سەپلەيدىغان "
#~ "مەركەزدۇر."

#~ msgid "Tree View"
#~ msgstr "دەرەخسىمان كۆرۈنۈش"

#~ msgid "Provides a classic tree-based view of control modules."
#~ msgstr "تىزگىنلەش بۆلەكلىرىنىڭ كلاسسىك دەرەخسىمان كۆرۈنۈشىنى تەمىنلەيدۇ."

#~ msgid "Expand the first level automatically"
#~ msgstr "بىرىنچى دەرىجىنى ئاپتوماتىك يايىدۇ"

#, fuzzy
#~| msgctxt "Search through a list of control modules"
#~| msgid "Search"
#~ msgid "Search..."
#~ msgstr "ئىزدە"

#, fuzzy
#~| msgid "System Settings"
#~ msgid "System Settings Handbook"
#~ msgstr "سىستېما تەڭشەكلىرى"

#, fuzzy
#~| msgid "About %1"
#~ msgid "About KDE"
#~ msgstr "%1 ھەققىدە"

#~ msgid "Overview"
#~ msgstr "قىسقىچە بايان"
